<?php
use App\Models\User;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    $users=User::get();
    $min=User::min('mark');
    $max=User::max('mark');
    return view('welcome',['users'=>$users,'min'=>$min,'max'=>$max]);
});
Route::post('/import',[UserController::class,'import'])->name('import');

