<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportUser;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function import(Request $request){
        $extension=$request->file('file')->extension();
        if($extension != 'xlsx'){
            return redirect()->back();
        }
        else{
            User::truncate();
            Excel::import(new ImportUser, $request->file('file')->store('files'));
            return redirect()->back();
        }

    }
}
