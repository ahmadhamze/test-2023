<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Test</title>
    <meta content="" name="description">
    <meta content="" name="keywords">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.5.4/css/colReorder.dataTables.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.dataTables.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.5.4/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">

    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">


    <link href="assets/css/main.css" rel="stylesheet">

</head>

<body>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Import File</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
            @csrf
              <input type="file" name="file" class="form-control">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success">Import</button>
            </div>
            </form>
          </div>
        </div>
    </div>

    <section id="hero" class="hero">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">
                <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>Welcome :)</h2>
                    <p>Import And Export And Ordering From Excel . </p>
                    <div class="d-flex justify-content-center justify-content-lg-start">
                        <button  class="btn-get-started" type="button" data-toggle="modal" data-target="#exampleModal">Import From Excel</button>
                    </div>

                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="assets/img/hero-img.svg" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="100">
                </div>
            </div>
        </div>
        @if ($users!='[]')
        <div class="icon-boxes position-relative">
            <div class="container position-relative">
                <div class="row gy-4 mt-5">

                    <div class="col-xl-4 col-md-4" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-arrow-down-square"></i></div>
                            <h5 class="title"><a class="stretched-link">The Lowest Mark For A Student</a></h5>
                            <h1 class="title"><a class="stretched-link">{{ $min }}</a></h1>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-alt"></i></div>
                            <h5 class="title"><a class="stretched-link">Count Of Student</a></h5>
                            <h1 class="title"><a class="stretched-link">{{ count($users) }}</a></h1>
                        </div>
                    </div>

                    <div class="col-xl-4 col-md-4" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-arrow-up-square"></i></div>
                            <h5 class="title"><a class="stretched-link">The Highest Mark For A Student</a></h5>
                            <h1 class="title"><a class="stretched-link">{{ $max }}</a></h1>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </section>

    <main id="main">

        <section>
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Student List</h2>
                </div>

                <div class="row gy-4">
                    {{-- <form action="{{ route('x') }}" method="POST" enctype="multipart/form-data">
@csrf --}}
                    <table class="myTable table table-striped">
                        <thead style="background-color: #008374; color: #fff;">
                            <tr>
                                <th style="padding-left: 50px;">Order</th>
                                <th>Name</th>
                                <th>Mark</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($users as $key => $user)
                            <tr>

                                <td style="padding-left: 50px;">{{ $user->id }} <i class="bi bi-distribute-vertical"></i>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->mark }}</td>
                            </tr>
                            @endforeach

                            </form>
                        </tbody>
                    </table>
                    {{-- <button type="submit" class="btn btn-success">sss</button> --}}

                </div>

            </div>
        </section>
        @endif
        {{-- <button id="btnn">Get </button> --}}
    </main>


</body>

    <div id="preloader"></div>

    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

    <script src="assets/js/main.js"></script>

    <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
    $(document).ready(function() {
        $('.myTable').DataTable({

            dom: 'Bfrtip',
            buttons: [

        { extend: 'csv', className: 'btn default',text:'Export To Excel'
            ,
            exportOptions: {

                columns: ':visible',

                modifier: { order: 'index' }

             },
    },
    ],
            searching: true,
            ordering: false,
            rowReorder: {
                update: true
            },
        });
//         $("#btnn").on("click", function () {
//     // var order_id = order.textContent;
//     $('#myTable').DataTable().ajax.reload();
//     var table =$('.myTable').DataTable();
//     new $.fn.dataTable.ColReorder( table, {
//         realtime: false
// } );
//     console.log(table.data());
//     var x = "";
//     $("#sortable li").each(function (index, element) {
//       x = x + $(this).text() + " , ";
//     });
//     $(".show").text(x);
//   });
    });

    </script>


</html>
